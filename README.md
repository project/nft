# Drupal NFT

## Introduction

This module aims to provide several tools to use NFTs in your Drupal site.

## Installation

Install as you would normally install a contributed Drupal module.

```
composer require drupal/nft
drush en nft
```

### Future developments

The following ideas are being discussed to extend NFT-related functionalities:

- Retrieve NFT from a given wallet
- Login with an NFT (because, why not?)
- Mint NFT from Drupal entities (e.g. NFT node owner)
- Mint NFT as a Commerce payment gateway

## Credits

The Nft icons was created by <a href="https://www.flaticon.com/free-icons/nft" title="nft icons">Konkapp - Flaticon</a>

## Maintainers

Current maintainers:

- [Matthieu Scarset](https://www.drupal.org/u/matthieuscarset)

This project has been initiated during NFT Hack 2022 by

- [ETHGlobal](https://showcase.ethglobal.com/)
