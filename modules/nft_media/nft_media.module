<?php

/**
 * @file
 * Custom functionalities for this module.
 */

use Drupal\Core\Url;
use Drupal\nft_media\Entity\NftMedia;

/**
 * Custom providers name.
 */
const NFT_OEMBED_PROVIDERS = ['Etherscan', 'OpenSea', 'Rarible'];

/**
 * Implements hook_entity_bundle_info_alter().
 */
function nft_media_entity_bundle_info_alter(array &$bundles): void {
  // Default class for NFT medias (e.g. enable `nft_media` module).
  if (isset($bundles['media']['nft'])) {
    $bundles['media']['nft']['class'] = NftMedia::class;
  }
}

/**
 * Implements hook_oembed_providers_alter().
 */
function nft_media_oembed_providers_alter(array &$providers) {
  // Dynamically register custom providers.
  // Required because endpoints point to this Drupal site.
  // Url may change dynamically so easier here than in configuration.
  $custom_providers = _nft_media_custom_providers_info();
  foreach ($custom_providers as $provider_name => $provider_info) {
    $providers[$provider_name] = $provider_info;
  }
}

/**
 * Get custom provider info.
 *
 * @return array
 *   The custom providers list.
 */
function _nft_media_custom_providers_info() {
  // Point all queries to our Drupal site.
  $endpoint_url = Url::fromRoute('nft_media.oembed')
    ->setAbsolute(TRUE)
    ->toString();

  $providers = [];

  // Etherscan.
  // @see https://docs.etherscan.io/api-endpoints/tokens
  $provider_name = 'Etherscan';
  $provider_url = 'https://etherscan.io';
  $providers[$provider_name] = [
    'provider_name' => $provider_name,
    'provider_url' => $provider_url,
    'endpoints' => [[
      'schemes' => [
        $provider_url . '/token/*',
        $provider_url . '/token/*?a=*',
      ],
      'url' => $endpoint_url,
      'discovery' => 'true',
    ]],
  ];

  // Polygon.
  // @see /token/0x47c7ff137d7a6644a9a96f1d44f5a6f857d9023f?a=43800
  $provider_name = 'Polygon';
  $provider_url = 'https://polygonscan.com';
  $providers[$provider_name] = [
    'provider_name' => $provider_name,
    'provider_url' => $provider_url,
    'endpoints' => [[
      'schemes' => [
        $provider_url . '/token/*',
        $provider_url . '/token/*?a=*',
      ],
      'url' => $endpoint_url,
      'discovery' => 'true',
    ]],
  ];

  // OpenSea.
  // @see https://docs.opensea.io/reference/api-overview
  $provider_name = 'OpenSea';
  $provider_url = 'https://opensea.io';
  $providers[$provider_name] = [
    'provider_name' => $provider_name,
    'provider_url' => $provider_url,
    'endpoints' => [
      [
        'schemes' => [
          $provider_url . '/assets/*/*',
        ],
        'url' => $endpoint_url,
        'discovery' => 'true',
      ],
    ],
  ];

  // Rarible.
  // @see https://ethereum-api.rarible.org/v0.1/doc
  $provider_name = 'Rarible';
  $provider_url = 'https://rarible.com';
  $providers[$provider_name] = [
    'provider_name' => $provider_name,
    'provider_url' => $provider_url,
    'endpoints' => [[
      'schemes' => [$provider_url . '/token/*:*'],
      'url' => $endpoint_url,
      'discovery' => 'true',
    ]],
  ];

  return $providers;
}
