<?php

namespace Drupal\nft_media\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\media\OEmbed\Provider;
use Drupal\media\OEmbed\Resource;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Web3\Validators\AddressValidator;

/**
 * Returns responses for nft_media module routes.
 */
class OEmbedController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Mime type helper.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected $mimeGuesser;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * The custom providers info.
   *
   * @var array
   */
  protected $providers;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->logger = $container->get('logger.channel.nft');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->httpClient = $container->get('http_client');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->fileSystem = $container->get('file_system');
    $instance->mimeGuesser = $container->get('file.mime_type.guesser.extension');
    $instance->fileUrlGenerator = $container->get('file_url_generator');

    $instance->providers = _nft_media_custom_providers_info();

    return $instance;
  }

  /****************************************************************************
   * API entry point.
   ****************************************************************************/

  /**
   * Read and process requested media Url.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The list of URL scheme as JSON.
   */
  public function endpoint() {
    $query = $this->request->query->all();
    $uri = $query['url'] ?? $query['uri'] ?? NULL;

    if (!UrlHelper::isValid($uri, TRUE)) {
      throw new \Exception('Invalid URL to get OEmbed content.');
    }

    // Get clean Url object with query.
    $parsed_url = parse_url($uri);
    $parsed_parts = explode('/', $parsed_url['path']);
    $parsed_url['query'] = $parsed_url['query'] ?? '';
    parse_str($parsed_url['query'], $parsed_queries);
    $cleaned_url = $parsed_url['scheme'] . '://';
    $cleaned_url .= $parsed_url['host'];
    $cleaned_url .= $parsed_url['path'];
    $remote_url = Url::fromUri($cleaned_url, [
      'query' => $parsed_queries,
    ]);

    // Find matching provider.
    $provider = NULL;
    foreach ($this->providers as $provider_name => $provider_info) {
      $parsed_provider_url = parse_url($provider_info['provider_url']);
      if ($parsed_url['host'] == $parsed_provider_url['host']) {
        $provider = $provider_name;
        break;
      }
    }

    // Keep values as context.
    $context = [];
    // Identify our call for the normalizer.
    $context['nft_media'] = TRUE;

    // @todo Would we allow other formats?
    $context['format'] = 'json';

    $context['remote_url'] = $remote_url;
    $context['provider_id'] = $provider;

    // @todo Allow blockchains selection.
    $context['blockchain'] = 'ETHEREUM'; // (default)
    // $context['blockchain'] = 'POLYGON';
    // $context['blockchain'] = 'FLOW';
    // $context['blockchain'] = 'TEZOS';

    // @todo Allow networks selection.
    $context['network_id'] = '1'; // mainnet (default)
    // $context['network_id'] = '4'; // rinkeby
    // $context['network_id'] = '5'; // görli
    // $context['network_id'] = '137'; // matic

    // @todo Loop through Url parts to find Contract address.
    //       We need to create a service to "guess" an address: "*\0x\".
    switch ($provider) {
      case 'Etherscan':
      case 'Polygon':
        $context['contract_address'] = $parsed_parts[2];
        $context['token_id'] = $parsed_queries['a'];
        break;
      case 'OpenSea':
        $context['contract_address'] = $parsed_parts[2];
        $context['token_id'] = $parsed_parts[3];
        break;
      case 'Rarible':
        $parts = explode(':', $parsed_parts[2]);
        $context['contract_address'] = $parts[0];
        $context['token_id'] = $parts[1];
        break;
      default;
        throw new \Exception('Error trying to identify the provider.');
    }

    if (!AddressValidator::validate($context['contract_address'])) {
      throw new \Exception('Invalid contract address in parsed URL: ' . $context['contract_address']);
    }

    // Get NFT info as a resource.
    $data = $this->getNftData($context);

    // Normalize values from remote data.
    $values = $this->normalizeData($data, $context);

    return new JsonResponse($values);
  }

  /****************************************************************************
   * Private methods.
   ****************************************************************************/

  /**
   * Presents the aggregator feed creation form.
   *
   * @param string $contract_address
   *   A given contract address (must start with '0x').
   * @param string $token_id
   *   A given token ID.
   * @param string $network_id
   *   A given network ID.
   * @param string $provider_id
   *   A given provider ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The normalized content as per OEmbed specifications.
   *
   * @see https://oembed.com/#section2.3.1
   */
  private function getNftData(array $context) {
    // @todo Load resource from cache.

    // @todo Make API key editable in config.
    $parameters = [];

    // @todo Make API endpoints editable in config.
    $args = [];

    $data = [];
    switch ($context['provider_id']) {
      case 'Etherscan':
      case 'OpenSea':
        // Using OpenSea.io SDK.
        // @see https://docs.opensea.io/reference/retrieving-a-single-asset
        $args[] = 'https://api.opensea.io/api/v1';
        $args[] = 'asset';
        $args[] = $context['contract_address'];
        $args[] = $context['token_id'];

        // @todo Separate custom module for OpenSea API.
        if ($api_key = getenv('opensea_api_key')) {
          $parameters[RequestOptions::HEADERS]['X-API-KEY'] = $api_key;
        }
        break;

      case 'Polygon':
        // Using NFTPort.xys API, if possible.
        // @see https://docs.opensea.io/reference/retrieving-a-single-asset
        if ($this->moduleHandler->moduleExists('nftport')) {
          $result = \Drupal::service('nftport.manager')->call('nfts', [
            'chain' => 'polygon',
            'contract_address' => $context['contract_address'],
            'token_id' => $context['token_id'],
          ]);
          if (!\is_array($result) || !isset($result['response']) || $result['response'] !== 'OK') {
            throw new \Exception('Error retrieving data from NFTPort API.');
          }
          if (!isset($result['nft']) || empty($result['nft'])) {
            throw new \Exception('Missing or empty NFT data from NFTPort API.');
          }
          return $result['nft'];
        } else {
          throw new \Exception(
            $this->t('Please install the `nftport` module to get NFT from Polygon (MATIC) network.')
          );
        }
        break;

      case 'Rarible':
        // Using Rarible Protocol API.
        // @see https://api.rarible.org/v0.1/doc
        $args[] = 'https://api.rarible.org/v0.1';
        $args[] = 'items';

        $item_id = [];
        $item_id[] = $context['blockchain'];
        $item_id[] = $context['contract_address'];
        $item_id[] = $context['token_id'];
        $args[] = implode(':', $item_id);
        break;
    }

    try {
      $remote_url = implode('/', $args);
      $response = $this->httpClient->get($remote_url, $parameters);
      $data = Json::decode($response->getBody()->getContents());
    } catch (Error $e) {
      $this->logger()->error($e->getMessage());
    }

    // @todo Save in cache.

    return $data;
  }

  /**
   * Prepare data compliant with Resource expectations.
   *
   * @param array $data
   *   A list of values.
   *
   * @return array
   *   An array of values ready to create a Resource object.
   */
  private function normalizeData(array $data, array $context) {
    $values = [];

    // Remove empty values for cleaner process.
    $data = array_filter($data);

    // Required hardcoded version.
    // @see \Drupal\media\OEmbed\ResourceFetcher::createResource();
    $values['version'] = '1.0';

    // Detect provider.
    $provider_id = $context['provider_id'] ?? NULL;
    $provider_info = $this->providers[$provider_id] ?? NULL;
    if ($provider_info) {
      $values['provider'] = new Provider(
        $provider_info['provider_name'],
        $provider_info['provider_url'],
        $provider_info['endpoints'],
      );
    }

    // Prepare values.
    switch ($provider_id) {
      case 'Etherscan':
      case 'OpenSea':
        $values['type'] = Resource::TYPE_PHOTO;
        $values['url'] = $data['image_url'];
        $values['title'] = $data['name'];
        $values['author_name'] = $data['creator']['user']['username'];
        $values['author_url'] = $data['asset_contract']['external_link'];
        $values['thumbnail_url'] = $this->getMediaUrl($data['image_thumbnail_url']);
        $values['thumbnail_width'] = 100;
        $values['thumbnail_height'] = 100;
        break;

      case 'Polygon':
        $metadata = $data['metadata'];
        $file_info = isset($data['file_information']) ? Json::decode($data['file_information']) : [];
        $image_url = $data['file_url'] ?? $data['image'] ?? $data['file_url'];
        $values['type'] = Resource::TYPE_PHOTO;
        $values['url'] = $image_url;
        $values['title'] = $metadata['name'];
        $values['author_name'] = NULL;
        $values['author_url'] = NULL;
        $values['thumbnail_url'] = $this->getMediaUrl($values['url']);
        $values['thumbnail_width'] = $file_info['width'] ?? 100;
        $values['thumbnail_height'] = $file_info['height'] ?? 100;
        break;

      case 'Rarible':
        $meta = $data['meta'];
        $content = $meta['content'];

        $values['type'] = Resource::TYPE_PHOTO;
        $values['title'] = $meta['name'];
        $values['description'] = $meta['description'];

        foreach ($content as $info) {
          // @todo Make acceptable image size editable in config.
          // $acceptable_image_size = 'ORIGINAL';
          $acceptable_image_size = 'BIG';
          switch ($info['representation']) {
            case $acceptable_image_size:
              $values['url'] = $this->getMediaUrl($info['url']);
              break;
            case 'PREVIEW':
              $values['thumbnail_url'] = $info['url'];
              $values['thumbnail_width'] = $info['width'];
              $values['thumbnail_height'] = $info['height'];
              break;
          }
        }

        break;
    }

    // Keep track of important original NFT info.
    $values['contract_address'] = $context['contract_address'];
    $values['token_id'] = $context['token_id'];

    // Safety first.
    foreach ($values as $key => $value) {
      if (\is_string($value)) {
        $values[$key] = Xss::filter($value);
      }
    }

    return $values;
  }

  /**
   * Handle mixed path to media.
   *
   * Helpful for images from IPFS (e.g. Rarible 'ORIGINAL').
   *
   * @param string $path
   *   A given path to a something online.
   *
   * @return string|null
   *   A Url.
   */
  private function getMediaUrl(string $remote_url = NULL) {
    if (!$remote_url) {
      return;
    }

    // Safety first.
    $sanitized_url = Xss::filter($remote_url);

    // Skip if filename looks okay.
    $mimetype = $this->mimeGuesser->guessMimeType($sanitized_url);
    if (strpos($mimetype, 'application/octet-stream') !== 0) {
      return $sanitized_url;
    }

    // Retrieve remote media locally.
    try {
      $local_url = $this->saveMedia($sanitized_url);
    } catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }

    return $local_url;
  }

  /**
   * Handle mixed path to media.
   *
   * Helpful for images from IPFS (e.g. Rarible 'ORIGINAL').
   *
   * @param string $path
   *   A given path to a something online.
   *
   * @return string
   *   A Url.
   *
   * @throws \Exception
   *   File system possibly does not manage to create directory or there might
   *   be an issue with data when saving in into a file. Most of the time, it is
   *   probably an issue with the file extension guessed.
   */
  private function saveMedia($remote_url) {
    $parsed_url = parse_url($remote_url);
    $parsed_parts = explode('/', $parsed_url['path']);
    $filename = end($parsed_parts);

    $response = $this->httpClient->get($remote_url);
    $mimetype = $response->getHeader('Content-Type')[0];
    $parsed_mimetype = explode('/', $mimetype);
    $extension = end($parsed_mimetype);
    $data = (string) $response->getBody();

    $directory = 'public://' . 'oembed_nft_media';
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);

    $replace = FileSystemInterface::EXISTS_REPLACE;
    $path = $directory . '/' . md5($filename) . '.' . $extension;
    $this->fileSystem->saveData($data, $path, $replace);

    return $this->fileUrlGenerator->generateAbsoluteString($path);
  }

}
