<?php

namespace Drupal\nft_media\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\media\Entity\Media;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Utility\Metadata;

class NftMedia extends Media implements NftInterface {

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['node'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Node'))
      ->setDescription(t('The related node entity, if it exists in Drupal.'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler_settings', ['target_bundles' => ['nft' => 'nft']]);

    return $fields;
  }

  /**
   * Retrieve the related NFT node entity, if it exists.
   *
   * @return \Drupal\nft\Entity\NftInterface|null
   *   The node entity for this NFT or nothing.
   */
  public function getNode() {
    return $this->get('node')->entity;
  }

  /**
   * {@inheritDoc}
   */
  public function getData(): Metadata {
    return ($node = $this->getNode()) instanceof NftInterface ? $node->getData() : new Metadata([]);
  }

  /**
   * {@inheritDoc}
   */
  public function setData(array $values = []) {
    if (($node = $this->getNode()) instanceof NftInterface) {
      $node->setData($values);
    }

    return $this;
  }

}
