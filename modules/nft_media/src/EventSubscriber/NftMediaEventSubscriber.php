<?php

namespace Drupal\nft_media\EventSubscriber;

use Drupal\Core\Queue\QueueFactory;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Event\MintEvent;
use Drupal\nft\Event\NftEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NftMediaEventSubscriber implements EventSubscriberInterface {

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Constructs a new OrderEventSubscriber object.
   */
  public function __construct(QueueFactory $queue_factory) {
    $this->queue = $queue_factory->get('nft_media_from_transaction');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[NftEvents::MINT] = 'onMint';
    return $events;
  }

  /**
   * Happens when a minter plugin finish its job.
   *
   * @param \Drupal\nft\Event\MintEvent $event
   *   The mint event with transaction details.
   */
  public function onMint(MintEvent $event) {
    // Create a queue item to create media out of tx.
    $nft = $event->getEntity();
    if ($nft instanceof NftInterface && $nft->isMinted()) {
      $this->queue->createItem($event->getTransaction());
    }
  }

}
