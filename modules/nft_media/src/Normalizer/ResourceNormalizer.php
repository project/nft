<?php

namespace Drupal\nft_media\Normalizer;

use Drupal\media\OEmbed\Resource;
use Drupal\serialization\Normalizer\ContentEntityNormalizer;

/**
 * Converts NFT data from/to OEmbed-compliant values/object.
 */
class ResourceNormalizer extends ContentEntityNormalizer {

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL) {
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    if ($data instanceof Resource) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($resource, $format = NULL, array $context = []) {
    $normalized_values = [];

    $normalized_values['type'] = $resource->getType();
    $normalized_values['title'] = $resource->getTitle();
    $normalized_values['author_name'] = $resource->getAuthorName();
    $normalized_values['author_url'] = $resource->getAuthorUrl();
    $normalized_values['provider'] = $resource->getProvider();
    $normalized_values['thumbnail_url'] = $resource->getThumbnailUrl();
    $normalized_values['thumbnail_width'] = $resource->getThumbnailWidth();
    $normalized_values['thumbnail_height'] = $resource->getThumbnailHeight();
    $normalized_values['width'] = $resource->getWidth();
    $normalized_values['height'] = $resource->getHeight();
    $normalized_values['url'] = $resource->getUrl();
    $normalized_values['html'] = $resource->getHtml();

    return $normalized_values;
  }

}
