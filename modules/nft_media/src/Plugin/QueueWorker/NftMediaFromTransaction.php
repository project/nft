<?php

namespace Drupal\nft_media\Plugin\QueueWorker;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Render\Markup;
use Drupal\nft\Transaction;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Try to create a NFT as media, for a given transaction.
 *
 * @QueueWorker(
 *  id = "nft_media_from_transaction",
 *  title = @Translation("NFT - Create a NFT as media from a transaction"),
 *  cron = {"time" = 30}
 * )
 */
class NftMediaFromTransaction extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The media storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $mediaStorage;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * Constructs a new NftMediaFromTransaction object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $bundle_info_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger;
    $this->mediaStorage = $entity_type_manager->getStorage('media');
    $this->bundleInfoService = $bundle_info_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.nft'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!$data instanceof Transaction) {
      $this->logger->error('NFT media bundle does not exists.');
      return;
    }

    $nft_bundles = array_filter($this->bundleInfoService->getBundleInfo('media'), function ($key) {
      return $key == 'nft';
    }, ARRAY_FILTER_USE_KEY);
    if (!($nft_bundle = reset($nft_bundles) ?? NULL)) {
      $this->logger->error('NFT media bundle does not exists.');
      return FALSE;
    }

    // @todo Get transaction status?
    throw new \Exception('Transaction pending. Requeued for later.');

    // Get NFT info as a resource.
    $context = [];
    $resource = $this->oembedManager->getNftData($context);

    $media = $this->mediaStorage->create([
      'type' => $nft_bundle,
      'name' => $name,
      'field_media_oembed_nft' => $url,
    ]);

    $violations = $media->validate();
    if ($violations->count()) {
      $violations_by_path = [];
      foreach ($violations as $violation) {
        $violations_by_path[$violation->getPropertyPath()][] = $violation->getMessage();
      }
      $this->logger->error('Error creating NFT media from transaction:<br><pre><code>@transaction</code></pre><br><pre><code>@errors</code></pre>' . [
        '@transaction' => Json::encode($transaction->getValues()),
        '@errors' => Markup::create('<ul><li>' . implode('</li><li>', $violations_by_path) . '</li></ul>'),
      ]);
    }

    $media->save();
  }

}
