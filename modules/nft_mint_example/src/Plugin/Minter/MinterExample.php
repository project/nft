<?php

namespace Drupal\nft_example\Plugin\Minter;

use Drupal\Component\Serialization\Json;
use Drupal\nft\Plugin\Minter\MinterBase;
use GuzzleHttp\RequestOptions;

/**
 * Defines a "example" minter plugin.
 *
 * @Minter(
 *   id = "minter_example",
 *   title = @Translation("Example name"),
 *   description = @Translation("Example description")
 * )
 */
class MinterExample extends MinterBase {

  /**
   * {@inheritDoc}
   */
  public function mint(Metadata $metadata): Transaction {
    try {
      // Call your service(s) to mint the NFT.
      $response = $this->httpClient->get('https://remote.service/api/create-nft', [
        RequestOptions::BODY => Json::encode($normalized_values),
      ]);
      $result = Json::decode($response->getBody());

      // Log the operation.
      $this->logger->notice($this->t('NFT was minted @token_id', [
        'token_id' => $result['token_id'] ?? NULL,
      ]));

      // Return a whatever suite you best.
      $transaction = new Transaction([
        'status' => TRUE,
        'message' => $this->t('What a nice NFT you have here! :D'),
        'metadata' => $metadata,
      ]);
    } catch (\Exception $e) {
      // Log failures.
      $this->logger->error($e->getMessage());

      // Return a failed transcation.
      $transaction = new Transaction([
        'status' => FALSE,
        'message' => $e->getMessage(),
        'metadata' => $metadata,
      ]);
    }

    return $transaction;
  }

}
