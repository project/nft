<?php

namespace Drupal\nft_example\Plugin\Minter;

use Drupal\nft\Plugin\Minter\MinterBase;

/**
 * Defines a "example" minter plugin.
 *
 * @Minter(
 *   id = "minter_example_with_custom_config",
 *   title = @Translation("Example custom"),
 *   description = @Translation("Example with custom configuration form."),
 *   forms = {
 *     "edit" = "\Drupal\nft_example\PluginForm\MinterExampleWithCustomConfigForm",
 *   }
 * )
 */
class MinterExampleWithCustomConfig extends MinterBase {
  // Nothing specific for this example.
  // Refer to the other example to learn how to implement your own "minting".
  // @see \Drupal\nft_example\Plugin\Minter\MinterExample::mint();
}
