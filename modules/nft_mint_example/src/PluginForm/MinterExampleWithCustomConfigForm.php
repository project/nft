<?php

namespace Drupal\nft_example\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\nft\PluginForm\MinterConfigFormBase;

/**
 * Form class for the `minter_example_with_custom_config` plugin.
 */
class MinterExampleWithCustomConfigForm extends MinterConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'custom_field' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['custom_field']['#type'] = 'link';
    $form['custom_field']['#title'] = $this->t('Custom thing');

    return $form;
  }

}
