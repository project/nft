<?php

/**
 * @file
 * Contains information about hooks defined in this module.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Edit existing Minter definitions.
 *
 * @param array $minters
 *   List of minters, passed by reference
 */
function hook_minter_info_alter(array &$minters) {
  // Do your things here.
}

/**
 * Alter contextual values before metadata normalization.
 *
 * @param array $context
 *   An array of values, passed by reference.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity being normalized.
 *
 * @see \Drupal\nft\Normalizer\EntityMetadataNormalizer::normalize();
 */
function hook_nft_metadata_context_alter(EntityInterface $entity, array &$context) {
  // Set the NFT's image from a field.
  if ($entity->hasField('field_image')) {
    $files = $entity->get('field_image')->referencedEntities();
    if ($file = reset($files)) {
      $context['image'] = \Drupal::service('file_url_generator')
        ->generateAbsoluteString($file->getFileUri());
    }
  }

  // Links to NFT metadata.
  // ----------------------
  // 1) Get metadata from built-in url.
  // Already done in the normalizer. Repeated here for demonstration purpose only.
  // Still a WIP (e.g https://www.drupal.org/project/nft/issues/3259265).
  if ($entity->hasLinkTemplate('nft-metadata')) {
    $metadata_file_url = $entity->toUrl('nft-metadata')
      ->setAbsolute(TRUE)
      ->toString();
  }

  // 2) Not recommended but works on public site: create a custom Url.
  // Must return a valid JSON response.
  // Not considered good practice as your site might be offline in the futur.
  // @see https://nftschool.dev/reference/metadata-schemas/#ethereum-and-evm-compatible-chains
  $metadata_file_url = Url::fromRoute('mymodule.controller_returning_json', [
    'entity' => $entity->id(),
  ])->setAbsolute(TRUE)->toString();

  // Pass "file_url" in context so that it is used during normalization.
  $context['file_url'] = $metadata_file_url;
}

/**
 * Alter normalized metadata.
 *
 * @param array $normalized_values
 *   The metadata, passed by reference.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity being normalized.
 * @param array $context
 *   An array of contextual values.
 *
 * @see \Drupal\nft\Plugin\Minter\MinterBase;
 * @see \Drupal\nft\Normalizer\EntityMetadataNormalizer::normalize();
 * @see https://docs.opensea.io/docs/metadata-standards#metadata-structure
 * @see https://docs.opensea.io/reference/asset-object#traits
 */
function hook_nft_metadata_values_alter(array &$values, EntityInterface $entity, array $context) {
  // Edit things.
  $values['description'] = t('This is a Drupal entity with ID: @id', [
    '@id' => $entity->id(),
  ]);

  // Remove things - maybe to avoid leaking "sensitive" information.
  unset($values['custom_fields']['entity_uuid']);

  // Add custom things (attributes/traits...etc).
  $values['attributes']['custom_attribute'] = 'custom_value';
}
