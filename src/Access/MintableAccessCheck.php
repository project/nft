<?php

namespace Drupal\nft\Access;

use Drupal\nft\Entity\NftInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access checker for mintable entity form.
 */
class MintableAccessCheck implements AccessInterface {

  /**
   * Checks access to the state transition confirmation form.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account) {
    // Get the entity type from the route name.
    // The entity route name is 'entity.{entity_type}.nft-mint'.
    $parts = explode('.', $route_match->getRouteName());
    $entity_type = $parts[1];
    $parameters = $route_match->getParameters();

    // Check if one of the required parameter is missing.
    if (!$parameters->has($entity_type)) {
      return AccessResult::neutral();
    }

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($entity_type);

    if ($entity instanceof NftInterface) {
      return AccessResult::forbidden();
    }

    return $entity->access('nft-mint', $account, TRUE);
  }

}
