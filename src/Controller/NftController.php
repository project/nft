<?php

namespace Drupal\nft\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handler nft routes.
 */
class NftController extends ControllerBase {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new MetadataController object.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  /**
   * Outputs an entity form to mint a new NFT from a given entity.
   *
   * @return array
   *   A render array.
   */
  public function mintForm() {
    // The entity route name is 'entity.{entity_type}.nft-mint'.
    $route_name = $this->routeMatch->getRouteName();
    $parts = explode('.', $route_name);
    $entity_type = $parts[1];
    $parameters = $this->routeMatch->getParameters();
    $entity = $this->routeMatch->getParameter($entity_type);

    // No need to check if entity is mintable here - already done.
    // @see \Drupal\nft\Routing\RouteSubscriber::alterRoutes()
    return $this->entityFormBuilder()->getForm($entity, 'nft_mint');
  }

  /**
   * Outputs admin report.
   *
   * @return array
   *   A render array
   */
  public function adminReport() {
    $build = [];

    return $build;
  }

}
