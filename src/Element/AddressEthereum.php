<?php

namespace Drupal\nft\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Element\Textfield;
use Web3\Validators\AddressValidator;

/**
 * Provides an ETH address form element.
 *
 * Usage example:
 * @code
 * $form['sender'] = [
 *   '#type' => 'address_ethereum',
 *   '#default_value' => '0x0000000000000000000000000000000000000000',
 * ];
 * @endcode
 *
 * @FormElement("address_ethereum")
 */
class AddressEthereum extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#size' => 43,
      '#maxlength' => 42,
      '#placeholder' => '0x0000000000000000000000000000000000000000',
      '#pattern' => '0x[a-fA-F\d]+',
      '#element_validate' => [[$class, 'validateAddress']],
    ] + parent::getInfo();
  }
  /**
   * Validation callback for a datelist element.
   *
   * @param array $element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public static function validateAddress(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = $element['#value'] ?? NULL;
    if (!empty($value) && !AddressValidator::validate($value)) {
      $form_state->setError($element, t('Invalid address @value', ['@value' => $value]));
    }
  }

}
