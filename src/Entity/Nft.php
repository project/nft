<?php

namespace Drupal\nft\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Utility\Metadata;
use Drupal\nft\Utility\Transaction;
use Drupal\node\Entity\Node;

class Nft extends Node implements NftInterface {

  use StringTranslationTrait;

  /**
   * Browse transaction history to see if NFT mint was successful.
   *
   * @return boolean
   */
  public function isMinted(): bool {
    $txs = $this->get('transactions')->getValue();
    foreach ($txs as $value) {
      $tx = Json::decode($value['value'] ?? '{}');
      if (($tx['status'] ?? NULL) === TRUE) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getData(): Metadata {
    $data = [];
    $data['name'] = $this->label();
    $data['description'] = $this->get('description')->value;
    $data['external_url'] = $this->get('external_url')->value;
    $data['image_url'] = $this->get('image_url')->value;
    $data['file_url'] = $this->get('file_url')->value;

    // 'animation_url';
    // 'youtube_url';
    // 'background_color';

    // Attributes.
    $attributes = [];
    foreach ($this->get('attributes')->getValue() as $item) {
      $attributes[] = Json::decode($item['value'] ?? '');
    }
    $attributes = array_filter($attributes);
    $data['attributes'] = !empty($attributes) ? $attributes : NULL;

    // Custom fields.
    $custom_fields = [];
    foreach ($this->get('custom_fields')->getValue() as $item) {
      if ($value = Json::decode($item['value'] ?? '')) {
        $custom_fields[key($value)] = current($value);
      }
    }
    $custom_fields = array_filter($custom_fields);
    $data['custom_fields'] = !empty($custom_fields) ? $custom_fields : NULL;

    return new Metadata($data);
  }

  /**
   * {@inheritDoc}
   */
  public function setData(array $values = []) {
    $metadata = new Metadata($values);
    foreach ($metadata->getValues() as $key => $value) {
      if ($key == 'name') {$key = 'title';}
      if ($this->hasField($key)) {
        if (\is_array($value)) {
          foreach ($value as $k => $v) {
            $this->get($key)->appendItem(Json::encode([$k => $v]));
          }
          continue;
        }

        $this->set($key, $value);
      }
    }

    return $this;
  }

}
