<?php

namespace Drupal\nft\Entity;

use Drupal\nft\Utility\Metadata;

/**
 * Defines getter and setter methods for NFT entitties.
 *
 * @ingroup nft
 */
interface NftInterface {

  /**
   * Gets the NFT info.
   *
   * @return \Drupal\nft\Utility\Metadata
   *   The metadata.
   */
  public function getData(): Metadata;

  /**
   * Set the NFT info.
   *
   * @param array $values
   *   A list of values.
   *
   * @return $this
   */
  public function setData(array $values = []);

}
