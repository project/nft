<?php

namespace Drupal\nft;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nft\Entity\Nft;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Form\MintEntityForm;
use Psr\Log\LoggerInterface;

/**
 * Manipulates entity type to rule the world.
 */
class EntityTypeInfo {

  use StringTranslationTrait;

  /**
   * A logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A messenger channel.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * Construct a new object.
   */
  public function __construct(
    LoggerInterface $logger,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfoService = $entity_type_bundle_info;
  }

  /*************************************************************************
   * Hooks.
   *************************************************************************/

  /**
   * Alter types to set custom link template on "mintable" entities.
   *
   * @param array &$entity_types
   *   List of entity types, passed by reference
   */
  public function entityTypeAlter(array &$entity_types) {
    foreach ($entity_types as $entity_type_id => $entity_type) {
      // Register "NFT Mint" route for all supported entity types.
      if ($entity_type->entityClassImplements(ContentEntityInterface::class)) {
        $base_path = $entity_type->hasLinkTemplate('canonical') ? $entity_type->getLinkTemplate('canonical') : NULL;
        $base_path = $base_path ?: ($entity_type->hasLinkTemplate('edit-form') ? $entity_type->getLinkTemplate('edit-form') : NULL);
        $base_path = $base_path ?: "/$entity_type_id/{$entity_type_id}";
        $entity_type->setLinkTemplate('nft-mint', $base_path . '/nft/mint');
      }

      // Register "NFT Mint" form mode for all mintable entity types.
      if ($entity_type->hasLinkTemplate('nft-mint') && !$entity_type->getFormClass('nft_mint')) {
        $entity_type->setFormClass('nft_mint', MintEntityForm::class);
      }
    }
  }

  /**
   * Adds nft operation on entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which to define an operation.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @see nft_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];

    // Display link to NFT mint operation, if possible.
    // @see nft_entity_access();
    // @see \Drupal\nft\EntityTypeInfo::entityOperationAccess();
    if (!$entity instanceof NftInterface && $entity->hasLinkTemplate('nft-mint') && $entity->access('nft-mint')) {
      $operations['nft_mint'] = [
        'title' => $this->t('Mint'),
        'weight' => 100,
        'url' => $entity->toUrl('nft-mint'),
      ];
    }

    return $operations;
  }

  /**
   * Implements a hook bridge for hook_entity_access().
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check access for.
   * @param string $operation
   *   The operation being performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account making the to check access for.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The result of the access check.
   *
   * @see nft_entity_access()
   *
   * @todo Implements dynamic permissions?
   */
  public function entityOperationAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'nft-mint') {

      // Avoid infinite "mint" loop.
      if ($entity instanceof NftInterface || !$entity->hasLinkTemplate('nft-mint')) {
        return AccessResult::forbidden();
      }

      $entity_type = $entity->getEntityType();
      $entity_type_id = $entity_type->id();
      $bundle = $entity_type->hasKey('bundle') ? $entity->bundle() : NULL;
      $mintable_permission = $bundle ? "mint $entity_type_id $bundle entity" : "mint $entity_type_id entity";

      // A minted NFT cannot be minted again, obviously.

      return AccessResult::forbiddenIf(!$account->hasPermission($mintable_permission));
    }

    return AccessResult::neutral();
  }

  /**
   * Customize NFT form.
   *
   * @param array $form
   *   The form, passed by reference.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  function nftFormAlter(&$form, FormStateInterface $form_state) {
    $node = $form_state->getFormObject()->getEntity();

    // Better UX.
    $form['metadata'] = [
      '#type' => 'details',
      '#title' => $this->t('Metadata'),
      '#open' => TRUE,
      '#weight' => 0,
    ];
    foreach (Element::children($form) as $key) {
      if ($key !== 'metadata') {
        $form[$key]['#group'] = ($form[$key]['#group'] ?? NULL) ?: 'metadata';
      }
    }

    // Disable values edition.
    if ($node->isMinted()) {
      $form['actions']['#access'] = FALSE;
      foreach (Element::children($form) as $key) {
        if ($node->hasField($key)) {
          $form[$key]['#disabled'] = TRUE;
        }
      }
      $this->messenger->addWarning($this->t('Content edition forbidden: @reason.', [
        '@reason' => $this->t('NFT already minted'),
      ]));
    }

    // Hide too much info from `nft_mint` entity form.
    if ($form_state->getFormObject() instanceof MintEntityForm) {
      $form['advanced']['#access'] = FALSE;
    }
  }

  /*************************************************************************
   * Callbacks.
   *************************************************************************/

  /**
   * Builds a list of custom permissions for this module.
   *
   * @return array
   *   The permissions.
   */
  public function buildPermissions() {
    $permissions = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
        continue;
      }
      if (!$entity_type->hasLinkTemplate('nft-mint')) {
      }

      $entity_type_id = $entity_type->id();
      if (!$entity_type->hasKey('bundle')) {
        // Access to "nft-mint" operation.
        $permissions["mint $entity_type_id entity"] = [
          'title' => $this->t('Mint @label as NFT', ['@label' => $entity_type->getSingularLabel()]),
          'description' => $this->t('Provides the ability to access "NFT Mint" form.'),
          'restrict access' => TRUE,
          'provider' => 'nft',
        ];
      }
      continue;
    }

    $bundles = $this->bundleInfoService->getBundleInfo($entity_type_id);
    foreach ($bundles as $bundle_id => $bundle_info) {
      // Access to "nft-mint" operation, by bundle.
      $permissions["mint $entity_type_id $bundle_id entity"] = [
        'title' => $this->t('Mint NFT from @bundle (@label)', [
          '@label' => ucfirst($entity_type->getSingularLabel()),
          '@bundle' => $bundle_info['label'],
        ]),
        'description' => $this->t('Provides the ability to access "NFT Mint" form.'),
        'restrict access' => TRUE,
        'provider' => 'nft',
      ];
    }

    return $permissions;
  }

}
