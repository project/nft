<?php

namespace Drupal\nft\Event;

use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Utility\Transaction;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the mint event.
 *
 * @see \Drupal\nft\Event\NftEvents
 */
class MintEvent extends Event {

  /**
   * The transaction received after the mint.
   *
   * @var \Drupal\nft\Utility\Transaction
   */
  protected $transaction;

  /**
   * The NFT node.
   *
   * @var \Drupal\nft\Entity\NftInterface
   */
  protected $entity;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\nft\Utility\Transaction $transaction
   *   The transaction object.
   * @param \Drupal\nft\Entity\NftInterface $node
   *   The transaction object.
   */
  public function __construct(Transaction $transaction, NftInterface $node) {
    $this->transaction = $transaction;
    $this->entity = $node;
  }

  /**
   * Gets the NFT node.
   *
   * @return \Drupal\nft\Entity\NftInterface
   *   The entity:node:nft.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Gets the transaction.
   *
   * @return \Drupal\nft\Utility\Transaction
   *   The transaction.
   */
  public function getTransaction() {
    return $this->transaction;
  }

}
