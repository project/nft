<?php

namespace Drupal\nft\Event;

/**
 * Defines events for this module.
 */
final class NftEvents {

  /**
   * Name of the event fired after a mint.
   *
   * @Event
   *
   * @see \Drupal\nft\Event\NftEvent
   */
  const MINT = 'nft.mint';

}
