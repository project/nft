<?php

namespace Drupal\nft\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nft\Event\MintEvent;
use Drupal\nft\Event\NftEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NftEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A messenger instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new OrderEventSubscriber object.
   */
  public function __construct(
    LoggerInterface $logger,
    MessengerInterface $messenger
  ) {
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[NftEvents::MINT] = 'onMint';
    return $events;
  }

  /**
   * Happens when a minter plugin finish its job.
   *
   * @param \Drupal\nft\Event\MintEvent $event
   *   The mint event with transaction details.
   */
  public function onMint(MintEvent $event) {
    $transaction = $event->getTransaction();

    // Log every mint transactions.
    $this->logger->notice($this->t('NFT mint: @status<br><pre><code>@transaction</code></pre>', [
      '@status' => $transaction->getStatus() ? $this->t('OK') : $this->t('KO'),
      '@transaction' => Json::encode($transaction->getValues()),
    ]));
  }

}
