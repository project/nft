<?php

namespace Drupal\nft\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Minter\MinterManagerInterface;
use Drupal\nft\Minter\MinterPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Mint NFT from a given entity.
 */
class MintEntityForm extends EntityForm {

  /**
   * The plugin manager.
   *
   * @var \Drupal\nft\Minter\MinterManagerInterface
   */
  protected $minterManager;

  /**
   * The plugin form manager.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * Constructs a new MintEntityForm object.
   *
   * @param \Drupal\nft\Minter\MinterManagerInterface $plugin_manager_minter
   *   The minter plugin manager service.
   * @param \Drupal\Core\Plugin\PluginFormFactoryInterface
   *   The plugin form factory.
   */
  public function __construct(MinterManagerInterface $plugin_manager_minter, PluginFormFactoryInterface $plugin_form_factory) {
    $this->minterManager = $plugin_manager_minter;
    $this->pluginFormFactory = $plugin_form_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.minter'),
      $container->get('plugin_form.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mint_' . $this->entity->getEntityTypeId() . '_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $wrapper_id = $this->getFormId() . '-ajax-wrapper';
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';
    $form['#tree'] = TRUE;

    $plugin_id = $form_state->getUserInput()['minter_id'] ?? NULL;
    $minter = $plugin_id ? $this->minterManager->createInstance($plugin_id) : NULL;

    // Plugin selection.
    $plugins = array_column($this->minterManager->getDefinitions(), 'title', 'id');
    asort($plugins);

    $form['minter_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Minter'),
      '#options' => $plugins,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $plugin_id,
      '#ajax' => [
        'wrapper' => $wrapper_id,
        'callback' => [get_called_class(), 'ajaxRefresh'],
      ],
      '#weight' => -1,
      '#required' => TRUE,
    ];

    $context = [];
    $context['form'] = $this->getFormId();
    $context['entity'] = $this->entity;
    $form_state->set('context', $context);

    // Display plugin's mint form.
    $form[$plugin_id] = ['#parents' => []];
    if ($plugin_form = $this->getPluginForm($minter, 'mint')) {
      $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
      $subform_state->set('entity', $this->entity);
      $subform_state->set('context', $form_state->get('context'));
      $form[$plugin_id] = $plugin_form->buildConfigurationForm($form[$plugin_id], $subform_state);
    }

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 1,
    ];

    $form['actions']['mint'] = [
      '#op' => 'mint',
      '#type' => 'submit',
      '#value' => $this->t('Create NFT'),
      '#validate' => ['::validateMintForm'],
      '#submit' => ['::submitMintForm'],
      '#states' => [
        'disabled' => [
          'select[name="minter_id"]' => ['value' => ''],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Ajax callback
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Get a plugin form.
   */
  public function getPluginForm($plugin, $operation = 'mint') {
    if ($plugin instanceof PluginWithFormsInterface) {
      if ($plugin->hasFormClass($operation)) {
        return $this->pluginFormFactory->createInstance($plugin, $operation);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No need to run validation over parent EntityForm.

    // Basic validations.
    if (!($plugin_id = $form_state->getValue('minter_id'))) {
      $form_state->setErrorByName('minter_id', $this->t('Missing minter ID'));
      return;
    }

    $minter = $this->minterManager->createInstance($plugin_id);
    if (!$minter instanceof MinterPluginInterface) {
      $form_state->setErrorByName('minter_id', $this->t('Error initializing the minter plugin'));
      return;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function validateMintForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $form_state->getValue('minter_id');
    $minter = $this->minterManager->createInstance($plugin_id);
    if ($plugin_form = $this->getPluginForm($minter, 'mint')) {
      if (!isset($form[$plugin_id])) {$form[$plugin_id] = ['#parents' => []];}
      $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
      $plugin_form->validateConfigurationForm($form[$plugin_id], $subform_state);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Disable everything from parent EntityForm.
  }

  /**
   * {@inheritDoc}
   */
  public function submitMintForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $form_state->getValue('minter_id');
    $minter = $this->minterManager->createInstance($plugin_id);
    $plugin_form = $this->getPluginForm($minter, 'mint');
    $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
    $plugin_form->submitConfigurationForm($form[$plugin_id], $subform_state);

    if (($nft = $form_state->get('nft')) instanceof NftInterface) {
      $form_state->setRedirectUrl($nft->toUrl('canonical'));
    }
  }
}
