<?php

namespace Drupal\nft\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure our custom settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'nft.settings';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->minterManager = $container->get('plugin.manager.minter');
    $instance->pluginFormFactory = $container->get('plugin_form.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nft_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $config_names = [];
    $config_names[] = static::SETTINGS;
    foreach (array_keys($this->minterManager->getDefinitions()) as $plugin_id) {
      $config_names[] = 'nft.minter.' . $plugin_id . '.settings';
    }
    return $config_names;
  }

  /**
   * Get a plugin form.
   */
  public function getPluginForm($plugin, $operation) {
    if ($plugin instanceof PluginWithFormsInterface) {
      if ($plugin->hasFormClass($operation)) {
        return $this->pluginFormFactory->createInstance($plugin, $operation);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $minters = $this->minterManager->getDefinitions();
    $plugins = array_column($minters, 'title', 'id');
    asort($plugins);

    $form['default_minter'] = [
      '#type' => 'select',
      '#title' => $this->t('Default minter'),
      '#options' => $plugins,
      '#default_value' => $config->get('default_minter'),
      '#required' => TRUE,
    ];

    $form['minters'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Minters'),
    ];

    // Build IPFS provider plugin form.
    foreach ($minters as $plugin_id => $plugin_info) {
      $form[$plugin_id . '_wrapper'] = [
        '#type' => 'details',
        '#title' => $plugin_info['title'],
        '#description' => $plugin_info['description'],
        '#group' => 'minters',
      ];

      $form['#parents'] = [];
      $form['#tree'] = TRUE;

      $form[$plugin_id] = ['#parents' => []];
      $plugin_settings = [];
      if ($plugin_config = $this->config('nft.minter.' . $plugin_id . '.settings')) {
        $plugin_settings = $plugin_config->getRawData();
      }
      $minter_plugin = $this->minterManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($minter_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $form[$plugin_id . '_wrapper'][$plugin_id] = $plugin_form->buildConfigurationForm($form[$plugin_id], $subform_state);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Save minter configurations.
    foreach (array_keys($this->minterManager->getDefinitions()) as $plugin_id) {
      $plugin_settings = $form_state->getValues()[$plugin_id] ?? [];
      $minter_plugin = $this->minterManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($minter_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->validateConfigurationForm($form[$plugin_id], $subform_state);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('default_minter', (string) $form_state->getValue('default_minter'))
      ->save();

    // Save minter configurations.
    foreach (array_keys($this->minterManager->getDefinitions()) as $plugin_id) {
      $plugin_settings = $form_state->getValues()[$plugin_id] ?? [];
      $minter_plugin = $this->minterManager->createInstance($plugin_id, $plugin_settings);
      if ($plugin_form = $this->getPluginForm($minter_plugin, 'edit')) {
        $subform_state = SubformState::createForSubform($form[$plugin_id], $form, $form_state);
        $plugin_form->submitConfigurationForm($form[$plugin_id], $subform_state);
      }

      $plugin_config = $this->configFactory->getEditable('nft.minter.' . $plugin_id . '.settings');
      foreach ($plugin_settings as $key => $value) {
        $plugin_config->set($key, $value);
      }
      $plugin_config->save();
    }

    parent::submitForm($form, $form_state);
  }

}
