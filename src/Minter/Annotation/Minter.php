<?php

namespace Drupal\nft\Minter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a minter annotation object.
 *
 * Plugin Namespace: Plugin\Minter
 *
 * @see plugin_api
 *
 * @Annotation
 */
class Minter extends Plugin {

  /**
   * The archiver plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the archiver plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the archiver plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * List of custom plugin forms.
   *
   * Default forms are already set, so just use this if you need a custom form.
   *
   * @see nft_minter_info_alter();
   * @see \Drupal\Core\Plugin\PluginWithFormTrait;
   *
   * @var array
   */
  public $forms;

  /**
   * Configuration to instanciate the IPFS provider plugin.
   *
   * Only useful if `ipfs` module installed.
   *
   * @see \Drupal\nft\Plugin\Minter\MinterBase::toUrl();
   *
   * @var array
   *
   * @code
   * ...
   *   ipfs = {
   *     "id" = "infura",
   *     "settings" = {},
   *   }
   * ...
   * @endcode
   */
  public $ipfs;

}
