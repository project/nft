<?php

namespace Drupal\nft\Minter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\nft\Minter\Annotation\Minter as MinterAnnotation;
use Drupal\nft\Minter\MinterPluginInterface;
use Drupal\nft\Minter\MinterManagerInterface;
use \Traversable as Traversable;

/**
 * Provides a Minter plugin manager.
 *
 * @see plugin_api
 */
class MinterManager extends DefaultPluginManager implements MinterManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Minter',
      $namespaces,
      $module_handler,
      MinterPluginInterface::class,
      MinterAnnotation::class
    );
    $this->alterInfo('minter_info');
    $this->setCacheBackend($cache_backend, 'minter_info_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    unset($definitions['broken']);
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginIds() {
    return array_keys($this->getDefinitions());
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'broken';
  }

}
