<?php

namespace Drupal\nft\Minter;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;

/**
 * Defines the common interface for all Minter manager classes.
 *
 * @see \Drupal\nft\Minter\MinterManager
 * @see plugin_api
 */
interface MinterManagerInterface extends FallbackPluginManagerInterface {

}
