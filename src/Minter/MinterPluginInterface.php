<?php

namespace Drupal\nft\Minter;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Executable\ExecutableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Url;
use Drupal\nft\Utility\Metadata;
use Drupal\nft\Utility\Transaction;

/**
 * Defines the common interface for all Minter classes.
 *
 * @see \Drupal\nft\Minter\MinterManager
 * @see \Drupal\nft\Minter\Annotation\Minter
 * @see plugin_api
 */
interface MinterPluginInterface extends PluginInspectionInterface, PluginWithFormsInterface, ExecutableInterface {

  /**
   * Build a form to allow custom logic in plugin's mint implementation.
   *
   * @param array $form
   *   The form.
   * @param FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form as render array.
   */
  public function buildMintForm(&$form, FormStateInterface $form_state);

  /**
   * Validation callback for mint form.
   *
   * @param array $form
   *   The form.
   * @param FormStateInterface $form_state
   *   The form state object.

   */
  public function validateMintForm(&$form, FormStateInterface $form_state);

  /**
   * Submit callback for mint form.
   *
   * @param array $form
   *   The form.
   * @param FormStateInterface $form_state
   *   The form state object.
   */
  public function submitMintForm(&$form, FormStateInterface $form_state);

  /**
   * Mint an Entity from normalized Metada values.
   *
   * @param \Drupal\nft\Utility\Metadata
   *   A normalized array of values
   * @param array $context
   *   Extra information.
   *
   * @return \Drupal\nft\Utility\Transaction
   *   A transaction object.
   */
  public function mint(Metadata $metadata, array $context = []): Transaction;

  /**
   * Generate NFT metadata for a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   (optional) A given entity.
   * @param array $context
   *   (optional) Extra information.
   *
   * @return \Drupal\nft\Utility\Metadata
   *   A metadata object.
   */
  public function metadata(EntityInterface $entity = NULL, array $context = []): Metadata;

  /**
   * Generate file URL from Metadata values and/or from a given entity.
   *
   * @param \Drupal\nft\Utility\Metadata
   *   A metadata object.
   * @param array $context
   *   Extra information.
   *
   * @return \Drupal\Core\Url
   *   The Url object.
   *
   * @throws \Exception
   */
  public function toUrl(Metadata $metadata, array $context = []): Url;

}
