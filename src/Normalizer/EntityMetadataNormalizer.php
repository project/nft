<?php

namespace Drupal\nft\Normalizer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\nft\Entity\NftInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Psr\Log\LoggerInterface;

/**
 * Converts NFT from/to Entity metadata.
 */
class EntityMetadataNormalizer extends NormalizerBase {

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a MetadataNormalizer object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    LoggerInterface $logger,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;

    // Allow custom NFT format only.
    $this->format = ['metadata'];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsNormalization($data, $format = NULL, array $context = []) {
    if (!is_object($data) || !$this->checkFormat($format)) {
      return FALSE;
    }

    // NFT metadata can only be generated out of a content.
    return $data instanceof ContentEntityInterface;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($entity, $format = NULL, array $context = []) {
    // Should not happens but who knows...
    if (!$entity instanceof ContentEntityInterface) {
      throw new \InvalidArgumentException(sprintf(
        '%s::%s function accepts ContentEntityInterface entities only: %s given.',
        static::class,
        __FUNCTION__,
        ($entity instanceof EntityInterface ? $entity->getEntityTypeId() : gettype($entity))
      ));
    }

    $context += $this->prepareContext($entity, $context);

    // Allow other modules to alter contextual values before normalization.
    $this->moduleHandler->alter('nft_metadata_context', $entity, $context);

    $values = [];

    // Basic fields.
    $values['name'] = $context['name'] ?? NULL;
    $values['description'] = $context['description'] ?? NULL;
    $values['external_url'] = $context['external_url'] ?? NULL;
    $values['image_url'] = $context['image_url'] ?? NULL;

    // Custom fields.
    $values['custom_fields'] = $context['custom_fields'] ?? NULL;

    // Attributes.
    $values['attributes'] = $context['attributes'] ?? NULL;

    // Publicly accessible file holding NFT metadata.
    $values['file_url'] = $context['file_url'] ?? NULL;

    // Allow other modules to alter metadata.
    $this->moduleHandler->alter('nft_metadata_values', $values, $entity, $context);

    // Other properties used by some APIs.
    // OpenSea.io and NFTPort.xyz expect `image_url` or `file_url` but others
    // services - such as NFTStorage.io - expects `image` or `file`, without `_url`.
    $values['image'] = $values['image_url'];
    $values['file'] = $values['file_url'];

    return $values;
  }

  /**
   * Populate context based on the given entity.
   *
   * @param mixed $entity
   *   The given entity.
   * @param array $context
   *   The context, passed by reference.
   *
   * @return array
   *   The contextual values.
   */
  private function prepareContext($entity) {
    // Get default values from existing NFT node.
    if ($entity instanceof NftInterface) {
      $metadata = $entity->getData();
      return array_filter($metadata->getValues());
    }

    $values = [];

    // Get default values from common content entities' methods.
    if ($entity instanceof ContentEntityInterface) {
      $entity_uuid = $entity->uuid();
      $entity_type = $entity->getEntityType();
      $entity_type_id = $entity->getEntityTypeId();
      $entity_bundle = $entity_type->hasKey('bundle') ? $entity->bundle() : $entity_type_id;
      $entity_url = $entity->hasLinkTemplate('canonical') ? $entity->toUrl('canonical')
        ->setAbsolute(TRUE)
        ->toString() : NULL;

      $entity_description = NULL;
      if ($entity->hasField('body') && !$entity->get('body')->isEmpty()) {
        $entity_description = text_summary(strip_tags($entity->get('body')->value));
      }
      $entity_description = $entity_description ?: t('A @entity_type from @site', [
        '@entity_type' => $entity_type->getSingularLabel(),
        '@site' => $this->configFactory->get('system.site')->get('name'),
      ]);

      // Basic information.
      $values['name'] = $entity->label();
      $values['description'] = $entity_description;
      $values['external_url'] = $entity_url;

      // @todo Map image with a given entity's field?
      $values['image_url'] = NULL;

      // @todo Map attributes with given entity's fields?
      $values['attributes'] = NULL;

      // Custom fields.
      $custom_fields = [];
      $custom_fields['entity_uuid'] = $entity_uuid;
      $custom_fields['entity_type'] = $entity_type_id;
      if ($entity_bundle) {
        $custom_fields['bundle'] = $entity_bundle;
      }
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      if ($storage instanceof RevisionableStorageInterface) {
        if ($revision_id = $storage->getLatestRevisionId($entity->id())) {
          $custom_fields['revision_id'] = $revision_id;
        }
      }
      $values['custom_fields'] = array_filter($custom_fields);
    }

    return array_filter($values);
  }
}
