<?php

namespace Drupal\nft\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for all entities.
 */
class NftLocalTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      // Register "NFT Mint" tab.
      if ($entity_type->hasLinkTemplate('nft-mint')) {
        $has_edit_path = $entity_type->hasLinkTemplate('edit_form');
        $has_canonical_path = $entity_type->hasLinkTemplate('canonical');

        if ($has_edit_path || $has_canonical_path) {
          $this->derivatives["$entity_type_id.nft-mint"] = [
            'route_name' => "entity.$entity_type_id.nft_mint",
            'title' => $this->t('NFT Mint'),
            'base_route' => "entity.$entity_type_id." . ($has_canonical_path ? "canonical" : "edit_form"),
            'weight' => 100,
          ];
        }
      }
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
