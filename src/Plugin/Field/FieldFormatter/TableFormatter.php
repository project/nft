<?php

namespace Drupal\nft\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'string_metadata' formatter.
 *
 * @FieldFormatter(
 *   id = "string_table",
 *   label = @Translation("Table"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class TableFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [
      '#theme' => 'table',
      '#header' => [$this->t('Key'), $this->t('Value')],
      '#rows' => [],
      '#empty' => t('Empty.'),
      '#attributes' => [
        'class' => [
          'string-table',
          'string-table--field-' . $items->getName(),
        ],
      ],
    ];

    $rows = [];
    foreach ($items as $delta => $item) {
      foreach (Json::decode($item->value) as $key => $value) {
        if (\is_array($value)) {
          $value = '<pre>' . json_encode($value) . '</pre>';
        }

        $row = [];
        $row[] = ['data' => ['#markup' => $key]];
        $row[] = ['data' => ['#markup' => $value]];
        $rows[] = $row;
      }
    }

    $elements['#rows'] = $rows;

    return $elements;
  }

}
