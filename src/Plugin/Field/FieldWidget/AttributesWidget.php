<?php

namespace Drupal\nft\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nft\Utility\Metadata;

/**
 * Plugin implementation of the 'string_metadata' widget.
 *
 * @FieldWidget(
 *   id = "string_nft_attributes",
 *   label = @Translation("NFT Attributes"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class AttributesWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = Json::decode($items[$delta]->value ?? '') ?? [];

    $element['display_type'] = [
      '#type' => 'select',
      '#empty_option' => $this->t('- Display type -'),
      '#options' => [
        'string' => $this->t('String'),
        'number' => $this->t('Number'),
        'boost_percentage' => $this->t('Percentage (%)'),
        'boost_number' => $this->t('Percentage (no sign)'),
        'date' => $this->t('Date'),
      ],
      '#placeholder' => $this->t('Display type'),
      '#default_value' => $values['display_type'] ?? 'string',
    ];
    $element['trait_type'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Trait type'),
      '#default_value' => $values['trait_type'] ?? NULL,
    ];
    $element['attribute_value'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Value'),
      '#default_value' => $values['value'] ?? NULL,
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      $array = [
        'display_type' => $value['display_type'] ?? NULL,
        'trait_type' => $value['trait_type'] ?? NULL,
        'value' => $value['attribute_value'] ?? NULL,
      ];
      $array = array_filter($array);
      $values[$delta] = !empty($array) ? Json::encode($array) : NULL;
    }

    return array_filter($values);
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return in_array($field_definition->getName(), ['attributes', 'custom_fields']);
  }

}
