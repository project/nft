<?php

namespace Drupal\nft\Plugin\Field\FieldWidget;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nft\Utility\Metadata;

/**
 * Plugin implementation of the 'string_metadata' widget.
 *
 * @FieldWidget(
 *   id = "string_key_value",
 *   label = @Translation("Key/Value string"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class KeyValueWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = Json::decode($items[$delta]->value ?? '') ?? [];

    $element['key'] = [
      '#type' => 'textfield',
      '#default_value' => key($values),
      '#placeholder' => $this->t('Key'),
    ];
    $element['value'] = [
      '#type' => 'textfield',
      '#default_value' => current($values),
      '#placeholder' => $this->t('Value'),
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      $k = $value['key'] ?? NULL;
      $v = $value['value'] ?? NULL;
      if ($k && $v) {
        $values[$delta] = Json::encode([$k => $v]);
      }
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return in_array($field_definition->getName(), ['attributes', 'custom_fields']);
  }

}
