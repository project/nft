<?php

namespace Drupal\nft\Plugin\Minter;

use Drupal\nft\Plugin\Minter\MinterBase;

/**
 * Defines a fallback minter plugin.
 *
 * @Minter(
 *   id = "broken",
 *   title = @Translation("Broken/Missing"),
 *   description = @Translation("This minter is broken or missing.")
 * )
 */
class Broken extends MinterBase {

}
