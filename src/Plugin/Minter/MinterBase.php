<?php

namespace Drupal\nft\Plugin\Minter;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\nft\Event\MintEvent;
use Drupal\nft\Event\NftEvents;
use Drupal\nft\Minter\MinterPluginInterface;
use Drupal\nft\Utility\Metadata;
use Drupal\nft\Utility\Transaction;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Base class for Minter plugins.
 */
abstract class MinterBase extends PluginBase implements MinterPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  use PluginWithFormsTrait;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration, $plugin_id, $plugin_definition,
    LoggerInterface $logger,
    ModuleHandlerInterface $module_handler,
    ClientInterface $http_client,
    SerializerInterface $serializer,
    EventDispatcherInterface $event_dispatcher,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
    $this->eventDispatcher = $event_dispatcher;
    $this->configFactory = $config_factory;

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('logger.channel.nft'),
      $container->get('module_handler'),
      $container->get('http_client'),
      $container->get('serializer'),
      $container->get('event_dispatcher'),
      $container->get('config.factory')
    );
  }

  /*************************************************************************
   * Configurable plugin methods
   *************************************************************************/

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $plugin_id = $this->getPluginId();
    $config_key = 'nft.minter.' . $plugin_id . '.settings';
    $plugin_config = $this->configFactory->get($config_key);

    return $plugin_config ? $plugin_config->getRawData() : [];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $default_config = $this->defaultConfiguration();
    $this->configuration = $default_config + $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /*************************************************************************
   * Overridable form methods
   * @see \Drupal\nft\PluginForm\MinterConfigFormBase
   *************************************************************************/

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginId();
    $settings = $this->getConfiguration();

    $form[$plugin_id] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
      '#weight' => 1,
      '#access' => !empty($settings),
    ];

    // Build form elements from configuration.
    foreach ($settings as $key => $value) {
      $form[$plugin_id][$key] = [
        '#type' => 'textfield',
        '#title' => $key,
        '#default_value' => $value,
        '#required' => TRUE,
        '#group' => $plugin_id,
      ];
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing specific to validate by default.
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->getValues()[$this->getPluginId()] ?? [];
    $this->setConfiguration($settings);
  }

  /**
   * {@inheritDoc}
   */
  public function buildMintForm(&$form, FormStateInterface $form_state) {
    // Nothing by default.
  }

  /**
   * {@inheritDoc}
   */
  public function validateMintForm(&$form, FormStateInterface $form_state) {
    // Nothing to validate by default.
  }

  /**
   * {@inheritDoc}
   */
  public function submitMintForm(&$form, FormStateInterface $form_state) {
    // Nothing specific to submit by default.
  }

  /*************************************************************************
   * Executable plugin methods
   *************************************************************************/

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    foreach ($entities as $entity) {
      $this->execute($entity);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function execute(EntityInterface $entity = NULL, array $context = []) {
    try {
      $metadata = $this->metadata($entity, $context);
      $transaction = $this->mint($metadata, $context);
      $this->event($transaction, $entity);
      return $transaction;
    } catch (\Exception$e) {
      $error = $e->getMessage();
      $this->logger->error($error);
      return new Transaction([
        'status' => FALSE,
        'message' => $error,
        'metadata' => $metadata ?? NULL,
        'context' => [
          'entity' => $entity ?? NULL,
        ],
      ]);
    }
  }

  /*************************************************************************
   * Custom minter logic
   *************************************************************************/

  /**
   * Tell the world about a minting event.
   *
   * @param \Drupal\nft\Utility\Transaction $transaction
   *   A given transaction object.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   (optional) The related entity.
   */
  public function event(Transaction $transaction, EntityInterface $entity = NULL) {
    $this->eventDispatcher->dispatch(new MintEvent($transaction, $entity), NftEvents::MINT);
  }

  /**
   * {@inheritDoc}
   */
  public function metadata(EntityInterface $entity = NULL, array $context = []): Metadata {
    $values = [];

    if ($entity instanceof EntityInterface) {
      $format = 'metadata';
      $context['minter'] = $this->getPluginId();
      $values = $this->serializer->normalize($entity, $format, $context);
    }

    return new Metadata($values + $context);
  }

  /**
   * {@inheritDoc}
   */
  public function mint(Metadata $metadata, array $context = []): Transaction {
    if (!Metadata::isValid($metadata->getValues())) {
      throw new \Exception($this->t('Invalid metadata: @values', [
        '@values' => Json::encode($metadata->getValues()),
      ]));
    }

    $transaction = new Transaction([
      'status' => FALSE,
      'message' => $this->t('`@plugin` minter was called.<br>Implements @function in your own plugin', [
        '@plugin' => $this->getPluginId(),
        '@function' => __FUNCTION__,
      ]),
      'metadata' => $metadata,
      'context' => $context,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function toUrl(Metadata $metadata, array $context = []): Url {
    $entity = $context['entity'] ?? $context['parent'] ?? NULL;

    // -------------------------------
    // Using IPFS storage for Metadata
    // -------------------------------
    // It is considered good practice to publish your NFTs' metadata as a file
    // on IPFS network so that it becomes `permanent` and `public`, forever.
    // But if your NFTs are somehow private, please disable this in settings.
    // Alternative option is to use your own localhost IPFS provider instance.
    $ipfs_settings = $this->getPluginDefinition()['ipfs'] ?? NULL;
    if ($this->moduleHandler->moduleExists('ipfs') && ($ipfs_provider_id = $ipfs_settings['id'] ?? NULL)) {
      try {
        $ipfs = \Drupal::service('plugin.manager.ipfs_provider')->createInstance(
          $ipfs_provider_id,
          $ipfs_settings['settings'] ?? []
        );

        $uuid = $entity instanceof EntityInterface ? $entity->uuid() :
        \Drupal::service('uuid')->generate();

        $filename = $entity->uuid() . '.metadata.json';
        $file_content = Json::encode(array_filter($metadata->getValues()));
        $cid = $ipfs->add($filename, $file_content);
        $file_url = $ipfs->toUrl($cid);
        if ($file_url instanceof Url) {
          return $file_url;
        }
      } catch (\Exception$e) {
        $this->logger->error($this->t('Error uploading file to IPFS from @plugin: @error', [
          '@plugin' => $this->getPluginId(),
          '@error' => $e->getMessage(),
        ]));
      }
    }

    // Return original Url, if possible.
    if ($current_url = $metadata->getValues()['file_url'] ?? NULL) {
      return Url::fromUri($current_url);
    }

    // Return link to the entity.
    if ($entity instanceof EntityInterface && $entity->hasLinkTemplate('canonical')) {
      return $entity->toUrl('canonical', ['query' => ['q' => 'metadata']])->setAbsolute(TRUE);
    }

    throw new \Exception('Impossible to get a metadata file url: @metadata', [
      '@metadata' => Json::encode($metadata->getValues()),
    ]);
  }

}
