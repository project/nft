<?php

namespace Drupal\nft\Plugin\Minter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\Plugin\Minter\MinterBase;
use Drupal\nft\Utility\Metadata;
use Drupal\nft\Utility\Transaction;
use Web3\Contract;
use Web3\Web3;

/**
 * Defines a minter plugin ready to talk with an ERC721 smart contract.
 *
 * @Minter(
 *   id = "erc721",
 *   title = @Translation("ERC721"),
 *   description = @Translation("Use an ERC721-compatible contract to mint NFTs."),
 *   ipfs = {
 *     "id" = "infura",
 *     "settings" = {},
 *   }
 * )
 */
class MinterERC721 extends MinterBase {

  /**
   * Web3 provider instance.
   *
   * @var \Web3\Web3
   */
  protected $web3;

  /**
   * The deployed contract.
   *
   * @var \Web3\Contract
   */
  protected $contract;

  /**
   *
   */
  public static function toHex($value) {
    if (\is_numeric($value)) {
      $value = dechex($value);
    }
    if (strpos($value, '0x') !== 0) {
      $value = '0x' . $value;
    }
    return $value;
  }

  /**
   * Separate function to easily identify custom config elements.
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'network_url' => '',
      'contract_address' => '',
      'contract_function' => 'createItem',
      'contract_abi' => '',
      'account_address' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $plugin_id = $this->getPluginId();
    $form[$plugin_id]['contract_abi']['#type'] = 'textarea';
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginId();
    // Reconnect with deployed contract.
    try {
      $this->connect()->getToken();
    } catch (\Exception$e) {
      $form_state->setErrorByName($plugin_id, $e->getMessage());
    }

    $tokens = $this->tokens ?? [];
    $token = reset($tokens) ?? [];
    if (!($token['name'] ?? NULL)) {
      $form_state->setErrorByName($plugin_id, 'Could not connect to contract.');
    }
  }

  /**
   * {@inheritDoc}
   */
  public function buildMintForm(&$form, FormStateInterface $form_state) {
    $form['tx'] = [
      '#type' => 'details',
      '#title' => $this->t('Transaction settings'),
      '#open' => TRUE,
      '#description' => $this->t('Numbers can be either in WEI or Hexadecimal.') . '<br>' .
      $this->t('Hex numbers will automatically be preprended: 0x'),
    ] + Transaction::formElements();

    $form['tx']['private_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('Will not be saved.') . ' ' . $this->t('Do not paste your seed phrase.') . ' ' .
      $this->t('Do not use this form if you do not trust this website.'),
    ];

    parent::buildMintForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateMintForm(&$form, FormStateInterface $form_state) {
    // Metadata always prepared.
    // @see \Drupal\nft\PluginForm\MinterMintFormBase::validateMintForm()
    $metadata = $form_state->get('metadata');

    // Pass custom tx arguments.
    // @see $this->buildMintForm()
    $context = $form_state->get('context') ?? [];
    $context += $form_state->getValues()['tx'] ?? [];

    try {
      // Run mint transaction.
      $transaction = $this->mint($metadata, $context);
      $form_state->set('transaction', $transaction);
      $this->logger->notice($transaction->toString());
    } catch (\Exception$e) {
      $form_state->setErrorByName(NULL, $e->getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitMintForm(&$form, FormStateInterface $form_state) {
    $result = [];
    $transaction = $form_state->get('transaction');

    try {
      $this->connect();
      $eth = $this->web3->getEth();
      $eth->getTransactionReceipt($transaction->get('hash'), function ($e, $receipt) use (&$result) {
        if ($e !== null) {throw new \Exception($e->getMessage());}
        $result = $receipt ?? [];
      });
    } catch (\Exception$e) {
      // Fail silently.
    }

    // Check transaction status.
    $transaction->set('result', (array) $result);

    $nft = $form_state->get('nft');
    if ($nft instanceof NftInterface && $nft->hasField('transactions')) {
      $nft->get('transactions')->appendItem($transaction->toString());
    }
  }

  /**
   * Connect to the deployed contract.
   *
   * @return $this
   */
  public function connect() {
    if (!$this->contract instanceof Contract) {
      $configuration = $this->getConfiguration();
      $network_url = $configuration['network_url'] ?? '';
      $contract_abi = Json::decode($configuration['contract_abi'] ?? '') ?? [];

      $this->web3 = new Web3($network_url);
      $this->contract = new Contract($this->web3->provider, $contract_abi);
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function mint(Metadata $metadata, array $context = []): Transaction {
    $this->connect();

    // Required "tokenURI": a public URL returning NFT's metadata as JSON.
    // @see https://docs.openzeppelin.com/contracts/4.x/erc721#constructing_an_erc721_token_contract
    if (!$token_uri = $metadata->getValues()['file_url'] ?? NULL) {
      $token_uri = $this->toUrl($metadata, $context);
      $metadata->set('file_url', $token_uri instanceof Url ? $token_uri->toString() : NULL);
    }

    $token_uri = $token_uri instanceof Url ? $token_uri->toString() : $token_uri;
    if (!$token_uri || empty($token_uri)) {
      throw new \Exception($this->t('Missing file_url to mint ERC721 NFT.'));
    }

    $transaction = new Transaction();
    $transaction->set('context', $context);
    $transaction->set('metadata', array_filter($metadata->getValues()));

    // Tx data.
    $data = array_merge($this->getConfiguration(), $context);
    $from = self::toHex($data['sender'] ?? NULL);
    $to = self::toHex($data['contract_address'] ?? NULL);
    $gas = self::toHex($data['gas'] ?? NULL);
    $gas_limit = self::toHex($data['gas_limit'] ?? NULL);
    $gas_price = self::toHex($data['gas_price'] ?? NULL);
    $value = self::toHex($data['value'] ?? NULL);

    $transaction->set('chainId', $data['chain_id'] ?? 1);
    $transaction->set('from', $from);
    $transaction->set('to', $to);
    $transaction->set('gas', $gas);
    $transaction->set('gasLimit', $gas_limit);
    $transaction->set('gasPrice', $gas_price);
    $transaction->set('value', $value);

    // Get nonce.
    $eth = $this->web3->getEth();
    if (!($nonce = $data['nonce'] ?? NULL)) {
      $eth->getTransactionCount($from, function ($e, $count) use (&$nonce) {
        if ($e !== null) {throw new \Exception($e->getMessage());}
        $nonce = $count->toHex();
      });
    }
    $transaction->set('nonce', self::toHex($nonce));

    // Get hex encoded call_data.
    $contract_address = $transaction->get('to');
    $encoded_data = $this->contract->at($contract_address)->getData(
      $data['contract_function'] ?? 'createItem',
      $from,
      $token_uri
    );
    $transaction->set('data', '0x' . $encoded_data);

    // Record transaction in log.
    $this->logger->notice($transaction->toString());

    // @todo Handle private key.
    $private_key = $data['private_key'] ?? NULL;
    $signature = $transaction->sign($private_key);
    $eth->sendRawTransaction('0x' . $signature, function ($e, $hash) use (&$transaction) {
      if ($e !== null) {throw new \Exception($e->getMessage() . ' ' . Json::encode($transaction->getTxData()));}
      $transaction->set('hash', $hash);
    });

    return $transaction;
  }

  /**
   * Get ERC721 token informatiom from contract.
   *
   * @return array
   *
   * @throws \Exception
   */
  public function getToken() {
    $this->connect();

    $configuration = $this->getConfiguration();
    $contract_address = $configuration['contract_address'] ?? NULL;

    $nft = [];
    $this->contract->at($contract_address)->call('name', [], function ($e, $data) use (&$nft) {
      if ($e) {throw new \Exception($e->getMessage());}
      $nft['name'] = reset($data);
    });
    $this->contract->at($contract_address)->call('symbol', [], function ($e, $data) use (&$nft) {
      if ($e) {throw new \Exception($e->getMessage());}
      $nft['symbol'] = reset($data);
    });

    // Save token information.
    $this->tokens = [$nft];

    return $this->tokens;
  }
}
