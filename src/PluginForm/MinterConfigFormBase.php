<?php

namespace Drupal\nft\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\nft\PluginForm\MinterPluginFormBase;

/**
 * A base form class for Minter plugin configuration.
 */
class MinterConfigFormBase extends MinterPluginFormBase {

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->plugin->buildConfigurationForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->submitConfigurationForm($form, $form_state);
  }

}
