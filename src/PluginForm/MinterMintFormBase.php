<?php

namespace Drupal\nft\PluginForm;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nft\Entity\NftInterface;
use Drupal\nft\PluginForm\MinterPluginFormBase;
use Drupal\nft\Utility\Metadata;
use Drupal\nft\Utility\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base form class for configurable Minter plugin execution.
 */
class MinterMintFormBase extends MinterPluginFormBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MinterMintFormBase constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // An NFT is required for plugin logic.
    if (!($nft = $form_state->get('nft')) instanceof NftInterface) {
      $entity = $form_state->get('entity');
      $context = $form_state->get('context') ?? [];
      $metadata = $this->plugin->metadata($entity, $context);

      $nft = $this->entityTypeManager->getStorage('node')
        ->create(['type' => 'nft'])
        ->setData($metadata->getValues());

      $form_state->set('nft', $nft);
      $form_state->set('metadata', $metadata);
    }

    $form['metadata'] = [
      '#type' => 'details',
      '#title' => $this->t('Metadata'),
      '#access' => $nft instanceof NftInterface,
    ];
    $form['metadata']['nft'] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'node',
      '#bundle' => 'nft',
      '#default_value' => $nft instanceof NftInterface ? $nft : NULL,
      '#form_mode' => 'nft_mint',
    ];

    $this->plugin->buildMintForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Something wrong happened with our NFT entit.
    if (!($nft = $form_state->get('nft')) instanceof NftInterface) {
      $form_state->setErrorByName(NULL, $this->t('Missing `nft` entity in @plugin_id form state', [
        '@plugin_id' => $this->getPluginId(),
      ]));
      return;
    }

    // Run custom plugin validation now.
    $this->plugin->validateMintForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->plugin->submitMintForm($form, $form_state);

    // Finally, create the NFT.
    $nft = $form_state->get('nft');
    if ($nft instanceof NftInterface) {
      $nft->save();
      $this->messenger()->addMessage($this->t('NFT @nid created.', ['@nid' => $nft->id()]));
      $form_state->setRedirectUrl($nft->toUrl('canonical'));
    }

    $nft = $form_state->get('nft');
    $transaction = $form_state->get('transaction');
    if ($transaction instanceof Transaction) {
      $this->plugin->event($transaction, $nft);
    }
  }

}
