<?php

namespace Drupal\nft\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * A base form class for Minter plugin forms.
 */
class MinterPluginFormBase extends PluginFormBase {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Return the plugin instance.
   *
   * @return MinterPluginInterface
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    $form_id = [];
    $form_id[] = 'minter';
    if ($plugin_id = $this->getPlugin()->getPluginId() ?? NULL) {
      $form_id[] = 'plugin';
      $form_id[] = $plugin_id;
    }
    $form_id[] = 'form';

    return implode('_', $form_id);
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
