<?php

namespace Drupal\nft\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for state machine transition routes on entities.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      // Register "NFT Mint" route for mintable entity types.
      if ($entity_type->hasLinkTemplate('nft-mint')) {
        $route = new Route($entity_type->getLinkTemplate('nft-mint'));
        $route
          ->setDefaults(['_controller' => '\Drupal\nft\Controller\NftController::mintForm'])
          ->setRequirement('_is_mintable', "TRUE")
          ->setRequirement($entity_type_id, '\d+')
          ->setOption('parameters', [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          ]);

        $collection->add("entity.$entity_type_id.nft_mint", $route);
      }
    }
  }

}
