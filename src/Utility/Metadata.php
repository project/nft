<?php

namespace Drupal\nft\Utility;

/**
 * Representation of a file containing details about an NFT, in the context of Drupal.
 *
 * @see https://nftschool.dev/reference/metadata-schemas/#erc-721
 * @see https://docs.opensea.io/docs/metadata-standards#metadata-structure
 * @see https://docs.opensea.io/docs/metadata-standards#attributes
 */
class Metadata {

  /**
   * Required attributes for a correct NFT metadata file.
   */
  const REQUIRED = ['name', 'description'];

  /**
   * Minimum attributes that an NFT should have.
   */
  const RECOMMENDED = ['name', 'description', 'external_url', 'image_url', 'file_url'];

  /**
   * Details of a transaction.
   *
   * @var array
   */
  protected $values;

  /**
   * Construct a new Transaction object.
   *
   * @param array $values
   *   The details of the transaction.
   */
  public function __construct(array $values = []) {
    $this->setValues($values);
  }

  /**
   * Get default values.
   *
   * @return array
   */
  public function defaultValues() {
    return [
      'name' => NULL,
      'description' => NULL,
      'external_url' => NULL,
      'animation_url' => NULL,
      'youtube_url' => NULL,
      'image_url' => NULL,
      'file_url' => NULL,
      'background_color' => NULL,
      'attributes' => [],
      'custom_fields' => [],
      'entities' => [],
    ];
  }

  /**
   * Get values of the metadata.
   *
   * @return array
   */
  public function getValues() {
    return $this->values + $this->defaultValues();
  }

  /**
   * Set content of the Metadata file.
   *
   * @param array $values
   *   (optional) A given list of values.
   */
  public function setValues(array $values = []) {
    $default_values = $this->defaultValues();
    $this->values = array_filter($values, function ($key) use ($default_values) {
      return in_array($key, array_keys($default_values));
    }, ARRAY_FILTER_USE_KEY) + $default_values;
  }

  /**
   * Set a single value.
   *
   * @param array $key
   * @param array $value
   */
  public function set(string $key, $value = NULL) {
    $this->values[$key] = $value;
  }

  /**
   * Check if an attribute is required for an NFT metadata.
   *
   * @return bool
   */
  public static function isRequired(string $key) {
    return in_array($key, self::REQUIRED);
  }

  /**
   * Check if an attribute is suggested to be included in a NFT metadata.
   *
   * @return bool
   */
  public static function isRecommended(string $key) {
    return in_array($key, self::RECOMMENDED);
  }

  /**
   * Check minimum metadata requirements as completed.
   *
   * @return bool
   */
  public static function isValid(array $values = []) {
    // Check required fields.
    foreach ($values as $key => $value) {
      if (self::isRequired($key) && (!$value || empty($value))) {
        return FALSE;
      }
    }

    return TRUE;
  }
}
