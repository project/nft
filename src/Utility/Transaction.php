<?php

namespace Drupal\nft\Utility;

use Drupal\Core\Render\Markup;
use Drupal\Component\Serialization\Json;
use Web3p\EthereumTx\Transaction as Tx;

/**
 * Representation of a Transaction to a Blockchain in the context of Drupal.
 */
class Transaction {

  /**
   * Details of a transaction.
   *
   * @var array
   */
  protected $values;

  /**
   * Construct a new Transaction object.
   *
   * @param array $values
   *   The details of the transaction.
   */
  public function __construct(array $values = []) {
    $this->values = $values + $this->defaultValues();
  }

  /******************************************************************
   * Internal/Drupal-related methods.
   ******************************************************************/

  /**
   * Get default values.
   *
   * @return array
   */
  public function defaultValues() {
    return $this->defaultTxData() + [
      // Drupal-related values.
      'status' => FALSE,
      'context' => [],
      'user' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * Get values of the transaction.
   *
   * @return array
   */
  public function getValues() {
    return $this->values;
  }

  /**
   * Update values of the transaction.
   *
   * @param array $values
   *   (optional) New value list.
   */
  public function setValues(array $values = []) {
    $this->values = $values;
  }

  /**
   * Set a single value.
   *
   * @param array $key
   * @param array $value
   */
  public function set(string $key, $value = NULL) {
    $this->values[$key] = $value;
  }

  /**
   * Get a single value.
   */
  public function get(string $key) {
    return $this->values[$key] ?? NULL;
  }

  /**
   * Get a single value.
   */
  public function toString() {
    return Json::encode($this->values);
  }

  /**
   * Build form elements to configure a "transaction".
   *
   * @return array
   *   The elements as render array.
   */
  public static function formElements() {
    $elements = [];

    $elements['chain_id'] = [
      '#type' => 'select',
      '#title' => t('Chain ID'),
      '#options' => [1 => 'Mainnet', 3 => 'Ropsten'],
      '#required' => TRUE,
    ];
    $elements['nonce'] = [
      '#type' => 'number',
      '#title' => t('Nonce'),
    ];
    $elements['gas'] = [
      '#type' => 'number',
      '#title' => t('Gas') . ' (' . t('in WEI') . ')',
      '#default_value' => 2310000,
    ];
    $elements['gas_limit'] = [
      '#type' => 'number',
      '#title' => t('Gas limit') . ' (' . t('in WEI') . ')',
      '#default_value' => 2310000,
    ];
    $elements['gas_price'] = [
      '#type' => 'number',
      '#title' => t('Gas price') . ' (' . t('in WEI') . ')',
      '#default_value' => 2000000000,
    ];
    $elements['value'] = [
      '#type' => 'number',
      '#title' => t('Value'),
      '#default_value' => 0,
    ];
    $elements['sender'] = [
      '#type' => 'address_ethereum',
      '#title' => t('Sender'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /******************************************************************
   * Ethereum-related methods.
   ******************************************************************/

  /**
   * Encode transaction data with a private key.
   *
   * @return string hex encoded signed ethereum transaction
   */
  public function sign(string $private_key) {
    $tx = new Tx($this->getTxData());
    return $tx->sign($private_key);
  }

  /**
   * Init a new web3 Tx.
   */
  public function getTxData() {
    $values = $this->getValues();
    $keys = array_keys($this->defaultTxData());
    $tx_data = array_filter($values, function ($key) use ($keys) {
      return in_array($key, $keys);
    }, ARRAY_FILTER_USE_KEY);

    return array_filter($tx_data);
  }

  /**
   * Get default values.
   *
   * @return array
   */
  public function defaultTxData() {
    return [
      // ETH-related values.
      'nonce' => NULL,
      'from' => NULL,
      'to' => NULL,
      'gas' => NULL,
      'gasPrice' => NULL,
      'value' => NULL,
      'chainId' => NULL,
      'data' => NULL,
    ];
  }

  /**
   * Get transaction status.
   */
  public function getStatus() {
    $status = ($this->values['result']['status'] ?? NULL) == '0x1';
    return $status;
  }
}
